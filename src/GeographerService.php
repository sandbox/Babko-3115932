<?php

namespace Drupal\yandex_weather;

use MenaraSolutions\Geographer\City;
use MenaraSolutions\Geographer\Earth;
use MenaraSolutions\Geographer\State;

/**
 * Class GeographerService.
 */
class GeographerService {

  protected $planet;

  protected $state;

  /**
   * Constructs a new GeographerService object.
   */
  public function __construct() {
    $this->planet = new Earth();
    $this->state = new State();
  }

  public function getCountriesOptionsList() {
    $countries = $this->initOption();
    $countries_objects = $this->planet->getCountries();
    foreach ($countries_objects as $countries_object) {
      $countries[$countries_object->getCode()] = $countries_object->getName();
    }
    return $countries;
  }

  public function getStatesOptionsList($country_code) {
    $states = $this->initOption();
    if ($country_code) {
      $country = $this->planet->findOne(['code' => $country_code]);
      $states_objects = $country->getStates();
      foreach ($states_objects as $states_object) {
        $states[$states_object->getCode()] = $states_object->getName();
      }
    }
    return $states;
  }

  public function getCitiesOptionsList($country_code, $state_code) {
    $cities = $this->initOption();
    if ($country_code) {
      $country = $this->planet->findOne(['code' => $country_code]);
      if ($state_code) {
        $state = $country->find(['code' => (int)$state_code])->first();
        $city_objects = $state->getCities();
        foreach ($city_objects as $city_object) {
          $cities[$city_object->getCode()] = $city_object->getName();
        }
      }
      else {
        $states_objects = $country->getStates();
        foreach ($states_objects as $states_object) {
          $city_objects = $states_object->getCities();
          foreach ($city_objects as $city_object) {
            $cities[$city_object->getCode()] = $city_object->getName();
          }
        }
      }

    }
    return $cities;
  }

  public function initOption() {
    return [
      '' => t('None'),
    ];
  }

}
