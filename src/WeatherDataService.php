<?php

namespace Drupal\yandex_weather;
//use Drupal\workflows\State;
use GuzzleHttp\ClientInterface;
use MenaraSolutions\Geographer\City;
use MenaraSolutions\Geographer\Earth;
use MenaraSolutions\Geographer\State;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class WeatherDataService.
 */
class WeatherDataService {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Base uri of yandex weather api.
   *
   * @var Drupal\yandex_weather
   */
  public static $baseUri = 'https://api.weather.yandex.ru/';

  /**
   * Constructs a new WeatherDataService object.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  public function prepareRequest($city) {
    $query = [];
    $cities_list = $this->getCitiesList();
    $query['lat'] = '55.75222';
    $query['lon'] = '37.61556';
    return $query;
  }

  public function getWeatherData($city) {
    try {
//      $api = \Drupal::config('yandex_weather.settings')->get('api_key');
//      $resp = $this->httpClient->request('GET', self::$baseUri . 'v1/informers',
//        [
//          'headers' => [
//            'X-Yandex-API-Key' => $api,
//          ],
//          'query' => $this->prepareRequest($city),
//        ]);
//
//      return $resp->getBody()->getContents();

    }
    catch (GuzzleException $exception) {
      watchdog_exception('yandex_weather', $exception);
      return FALSE;
    }
  }

  public function getCitiesList() {
    $cities = [];
    $planet = new Earth();
    $countries = $planet->getCountries();
    foreach ($countries as $country) {
      $states = $country->getStates();
      foreach ($states as $state) {
        $state_cities = $state->getCities();
        foreach ($state_cities as $state_city) {
          $cities[] = $state_city->toArray();
        }
      }
    }
    return $cities;
  }

  public function findCities($city) {
    $cities = [];
    $planet = new Earth();
    $countries = $planet->getCountries();
    foreach ($countries as $country) {
      $states = $country->getStates();
      foreach ($states as $state) {
        $sss = $state->getCities();
        $cities[] = $sss->find(['name' => $city]);
      }
    }
    return $cities;
  }

}
