<?php

namespace Drupal\yandex_weather\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\yandex_weather\WeatherDataService;
use Drupal\yandex_weather\GeographerService;

/**
 * Provides a 'WeatherBlock' block.
 *
 * @Block(
 *  id = "weather_block",
 *  admin_label = @Translation("Weather block"),
 * )
 */
class WeatherBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\yandex_weather\WeatherService
   */
  protected $weatherservice;

  /**
   * The module handler.
   *
   * @var \Drupal\yandex_weather\GeographerService
   */
  protected $geographerservice;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
    ] + parent::defaultConfiguration();
  }

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   *
   * @var string $weatherservice
   *   The information from the Weather service for this block.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, WeatherDataService $weatherservice, GeographerService $geographerservice) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->weatherservice = $weatherservice;
    $this->geographerservice = $geographerservice;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('yandex_weather.weather_data'),
      $container->get('yandex_weather.geographer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $current_state = $form_state->getCompleteFormState();
    $trig_el = $current_state->getTriggeringElement();
    $settings = $current_state->getValue('settings');
    if ($trig_el["#name"] === "settings[country]") {
      $settings['country_state'] = '';
      $settings['states_city'] = '';
    }
    if ($trig_el["#name"] === "settings[country_state]") {
      $settings['states_city'] = '';
    }


    $form['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#options' => $this->geographerservice->getCountriesOptionsList(),
      '#ajax' => [
        'callback' => [$this, 'getCountryStates'],
        'event' => 'change',
        'wrapper' => 'country-states',
      ]
    ];

    $country_state_options = $this->geographerservice->getStatesOptionsList($settings['country']);

    $form['country_state'] = [
      '#type' => 'select',
      '#title' => $this->t('State'),
      '#options' => $country_state_options,
      '#prefix' => '<div id="country-states">',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => [$this, 'getStatesCity'],
        'event' => 'change',
        'wrapper' => 'states-city'
      ]
    ];

    $city_state_options = $this->geographerservice->getCitiesOptionsList($settings['country'], $settings['country_state']);

    $form['states_city'] = [
      '#type' => 'select',
      '#title' => $this->t('City'),
      '#options' => $city_state_options,
      '#description' => $this->t('Enter City'),
      '#prefix' => '<div id="states-city">',
      '#suffix' => '</div>',
    ];

    return $form;
  }

  public function getCountryStates($form, FormStateInterface $form_state) {
    return $form['settings']['country_state'];
  }

  public function getStatesCity($form, FormStateInterface $form_state) {
    return $form['settings']['states_city'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['weather_city'] = $form_state->getValue('weather_city');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'weather_block';
    //$weather_date = $this->weatherservice->getWeatherData("Москва");
    //$weather_date = $this->weatherservice->getCitiesList();
    $weather_date = $this->weatherservice->findCities("Moskow");

    $build['#content'][] = $this->configuration['weather_city'];
    return $build;
  }

}
